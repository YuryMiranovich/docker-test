package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DockerTestAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerTestAppApplication.class, args);
    }

    @GetMapping("/test")
    public String getTest(){
        return "test endpoint is working";
    }
}
