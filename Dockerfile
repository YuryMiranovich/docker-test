FROM openjdk:8-alpine
WORKDIR /app
COPY . .
RUN ./gradlew build
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=production", "-Xmx512m",  "build/libs/printservice-WIP.jar"]
EXPOSE 8080